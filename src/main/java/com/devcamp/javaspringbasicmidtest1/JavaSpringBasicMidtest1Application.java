package com.devcamp.javaspringbasicmidtest1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSpringBasicMidtest1Application {

	public static void main(String[] args) {
		SpringApplication.run(JavaSpringBasicMidtest1Application.class, args);
	}

}
