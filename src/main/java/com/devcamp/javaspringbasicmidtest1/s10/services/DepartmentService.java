package com.devcamp.javaspringbasicmidtest1.s10.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.javaspringbasicmidtest1.s10.models.Department;
import com.devcamp.javaspringbasicmidtest1.s10.models.Staff;

import java.util.*;

@Service
public class DepartmentService {
    @Autowired
    StaffService staffService;

    public ArrayList<Department> getAllDepartmentService(){
        ArrayList<Department> allDeparment = new ArrayList<>();
        ArrayList<Staff> staffRom1 = staffService.getStaffRom1();
        ArrayList<Staff> staffRom2 = staffService.getStaffRom2();
        ArrayList<Staff> staffRom3 = staffService.getStaffRom3();

        allDeparment.add(new Department(1, "rom-1", "Q1.TpHCM", staffRom1));
        allDeparment.add(new Department(2, "rom-2", "Q2.TpHCM", staffRom2));
        allDeparment.add(new Department(3, "rom-3", "Q2.TpHCM", staffRom3));

        return allDeparment;
    }

    public Department getDepartmentByIdService(int id) {
        Department result = new Department();
        for (Department departmentElement : getAllDepartmentService()) {
            if(departmentElement.getId() == id){
                result = departmentElement;
            }
        }
        return result;
    }

    public ArrayList<Department> getaverageAgeService(float averageAge){
        ArrayList<Department> result = new ArrayList<>();

        for (Department departmentElement : getAllDepartmentService()) {
            if(departmentElement.getAverageAge() > averageAge){
                result.add(departmentElement);
            }
        }

        return result;
    }
}
