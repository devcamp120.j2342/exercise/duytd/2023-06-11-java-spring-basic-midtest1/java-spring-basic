package com.devcamp.javaspringbasicmidtest1.s10.services;

import org.springframework.stereotype.Service;

import com.devcamp.javaspringbasicmidtest1.s10.models.Staff;

import java.util.*;

@Service
public class StaffService {

    Staff staff1 = new Staff(1, "Đặng Vương Hưng", 26);
    Staff staff2 = new Staff(2, "Nguyễn Phong", 27);
    Staff staff3 = new Staff(3, "Trần Thanh Hải", 28);

    Staff staff4 = new Staff(1, "Thích Nhất Hạnh", 26);
    Staff staff5 = new Staff(2, "Kito Aya", 25);
    Staff staff6 = new Staff(3, "Cảnh Thiên", 26);

    Staff staff7 = new Staff(1, "Nguyễn Việt Long", 25);
    Staff staff8 = new Staff(2, "Majaa", 27);
    Staff staff9 = new Staff(3, "Lê Hải Đoàn", 29);

    public ArrayList<Staff> getStaffRom1() {
        ArrayList<Staff> staffRom1 = new ArrayList<>();
        staffRom1.add(staff1);
        staffRom1.add(staff2);
        staffRom1.add(staff3);

        return staffRom1;
    }

     public ArrayList<Staff> getStaffRom2() {
        ArrayList<Staff> staffRom2 = new ArrayList<>();
        staffRom2.add(staff4);
        staffRom2.add(staff5);
        staffRom2.add(staff6);

        return staffRom2;
    }

     public ArrayList<Staff> getStaffRom3() {
        ArrayList<Staff> staffRom3 = new ArrayList<>();
        staffRom3.add(staff7);
        staffRom3.add(staff8);
        staffRom3.add(staff9);

        return staffRom3;
    }

    public ArrayList<Staff> getAllStaffService() {
        ArrayList<Staff> allStaff = new ArrayList<>();

        allStaff.add(staff1);
        allStaff.add(staff2);
        allStaff.add(staff3);

        allStaff.add(staff4);
        allStaff.add(staff5);
        allStaff.add(staff6);

        allStaff.add(staff7);
        allStaff.add(staff8);
        allStaff.add(staff9);

        return allStaff;
    }

    public ArrayList<Staff> getStaffByAgeService(int age) {
        ArrayList<Staff> result = new ArrayList<>();

        for (Staff staffElement : getAllStaffService()) {
            if (staffElement.getAge() > age) {
                result.add(staffElement);
            }
        }

        return result;
    }

}
