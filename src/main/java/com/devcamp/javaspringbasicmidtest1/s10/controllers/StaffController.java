package com.devcamp.javaspringbasicmidtest1.s10.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.javaspringbasicmidtest1.s10.models.Staff;
import com.devcamp.javaspringbasicmidtest1.s10.services.StaffService;

import java.util.*;
@RestController
@CrossOrigin
@RequestMapping("/api")
public class StaffController {
    @Autowired 
    StaffService staffService;

    @CrossOrigin
    @GetMapping("/staffs")
    public ArrayList<Staff> getAllStaffs(){
        return staffService.getAllStaffService();
    }

    @CrossOrigin
    @GetMapping("/staff/{age}")
    public ArrayList<Staff> getStaffByAge(@PathVariable int age){
        return staffService.getStaffByAgeService(age);
    }
}
