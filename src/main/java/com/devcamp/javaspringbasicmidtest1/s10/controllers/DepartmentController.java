package com.devcamp.javaspringbasicmidtest1.s10.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

import com.devcamp.javaspringbasicmidtest1.s10.models.Department;
import com.devcamp.javaspringbasicmidtest1.s10.services.DepartmentService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class DepartmentController {
    @Autowired
    DepartmentService departmentService;

    @CrossOrigin
    @GetMapping("/department")
    public ArrayList<Department> getAllDepartment() {
        return departmentService.getAllDepartmentService();
    }

    @CrossOrigin
    @GetMapping("/department/{id}")
    public Department getDepartmentById(@PathVariable("id") int id) {
        return departmentService.getDepartmentByIdService(id);
    }

    @CrossOrigin
    @GetMapping("/averageAge/{averageAge}")
    public ArrayList<Department> getaverageAge (@PathVariable("averageAge") float averageAge){
        return departmentService.getaverageAgeService(averageAge);
    }
}
