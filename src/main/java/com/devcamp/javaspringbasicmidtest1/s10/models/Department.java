package com.devcamp.javaspringbasicmidtest1.s10.models;

import java.util.*;

public class Department {

    private int id;
    private String name;
    private String address;
    private ArrayList<Staff> staff;

    public Department() {
    }

   

    public Department(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }



    public Department(int id, String name, String address, ArrayList<Staff> staff) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.staff = staff;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<Staff> getStaff() {
        return staff;
    }

    public void setStaff(ArrayList<Staff> staff) {
        this.staff = staff;
    }

    public float getAverageAge() {
        int totalAge = 0;
        int numberOfStaff = staff.size();

        for (Staff s : staff) {
            totalAge += s.getAge();
        }

        float averageAge = (float) totalAge / numberOfStaff;
        return averageAge;

    }

    @Override
    public String toString() {
        return "Department [id=" + id + ", name=" + name + ", address=" + address + ", staff=" + staff + "]";
    }

}
