package com.devcamp.javaspringbasicmidtest1.s20.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

@Service
public class StringAPIService {

    public String getApi1(String input) {
        StringBuilder reversed = new StringBuilder(input);
        reversed.reverse();
        return reversed.toString();
    }

    public String checkPalindrome(String input) {
        String reversed = new StringBuilder(input).reverse().toString();

        if (input.equals(reversed)) {
            return "Chuỗi \"" + input + "\" là một palindrome.";
        } else {
            return "Chuỗi \"" + input + "\" không phải là palindrome.";
        }
    }

    public String removeDuplicateCharacters(String input) {
        Set<Character> seenCharacters = new HashSet<>();
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char currentChar = input.charAt(i);

            if (!seenCharacters.contains(currentChar)) {
                seenCharacters.add(currentChar);
                result.append(currentChar);
            }
        }

        return result.toString();
    }

    public String concatAndTrim (String str1, String str2) {
        
        int length1 = str1.length();
        int length2 = str2.length();

        if(length1 > length2) {
            str1 = str1.substring(length1 - length2); 
        }else if( length2 > length1) {
            str2 = str2.substring(length2 - length1);
        }
        
        return str1.concat(str2);
    }
}
