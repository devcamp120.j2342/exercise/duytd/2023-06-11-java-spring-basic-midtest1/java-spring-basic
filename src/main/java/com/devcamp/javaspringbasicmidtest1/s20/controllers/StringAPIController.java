package com.devcamp.javaspringbasicmidtest1.s20.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.javaspringbasicmidtest1.s20.services.StringAPIService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class StringAPIController {
    @Autowired
    StringAPIService stringApiService;

    @CrossOrigin
    @GetMapping("/stringapi")
    public String getStringAPI(@RequestParam("input") String input) {
        return stringApiService.getApi1(input);
    }

    @CrossOrigin
    @GetMapping("/stringapi2")
    public String getStringPalindrome(@RequestParam("input") String input) {
        return stringApiService.checkPalindrome(input);
    }

    @CrossOrigin
    @GetMapping("/stringapi3")
    public String removeDuplicateChar(@RequestParam("input") String input) {
        return stringApiService.removeDuplicateCharacters(input);
    }

    @CrossOrigin
    @GetMapping("/stringapi4")
    public String concatAndTrim(@RequestParam("str1") String str1, @RequestParam("str2") String str2){
        return stringApiService.concatAndTrim(str1, str2);
    }
}
